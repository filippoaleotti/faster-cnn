# --------------------------------------------------------
# Fast R-CNN
# Copyright (c) 2015 Microsoft
# Licensed under The MIT License [see LICENSE for details]
# Written by Ross Girshick
# --------------------------------------------------------

import os

import sys
sys.path.insert(0, './../')

from datasets.imdb import imdb
import datasets.ds_utils as ds_utils
import xml.etree.ElementTree as ET
import numpy as np
import scipy.sparse
import scipy.io as sio
import utils.cython_bbox
import pickle
import subprocess
import uuid
from datasets.my_dataset_eval import my_dataset_eval
from fast_rcnn.config import cfg
import pdb
from utils.utils_v2 import *

class my_dataset(imdb):
    def __init__(self, image_set):
        imdb.__init__(self, 'my_dataset' + '_' + image_set)
        self._image_set = image_set
        self.my_cfg = get_configuration_parameters()

        #recupero le classi dal file di configurazione facendo attenzione
        #a lasciare sempre background come prima classe
        self._classes = ['__background__']
        for c in self.my_cfg["classes"]:
            self._classes.append(c)
        print (("classi utilizzate {}").format(self._classes))

        self._num_classes = len(self._classes)
        #creo un dizionario di coppie intero, nome_classe
        self._class_to_ind = dict(zip(self._classes, range(self._num_classes)))

        #recupero il path al file che contiene la lista delle immagini
        #dal file di configurazione json
        if image_set == "test":
            self._image_path_txt = self.my_cfg["test_image_txt"]
        elif image_set == "train":
            self._image_path_txt = self.my_cfg["train_image_txt"]
        elif image_set == "valid":
            self._image_path_txt = self.my_cfg["valid_image_txt"]
        else:
            print("nessuno dei parametri utilizzati risulta valido")
            print("sono supportate le sole operazioni di train, test e valid")
            exit()

        #recupero la lista delle immagini da utilizzare
        self._image_index = self._load_image_set_index()

        #recupero le bounding boxes associate alle immagini
        self._roidb_handler = self.gt_roidb
        self._salt = str(uuid.uuid4())
        self._comp_id = 'comp4'

        # PASCAL specific config options
        self.config = {'cleanup'     : True,
                       'use_salt'    : True,
                       'use_diff'    : False,
                       'matlab_eval' : False,
                       'rpn_file'    : None,
                       'min_size'    : 2}

    def image_path_at(self, i):
        """
        Return the absolute path to image i in the image sequence.
        """
        return (self._image_index[i])

    def _load_image_set_index(self):
        """
        Load the indexes listed in this dataset's image set file.
        """
        #recupera il nome delle immagini presenti nel file di testo txt
        #specificato nella configurazione
        image_set_file = self._image_path_txt
        assert os.path.exists(image_set_file), \
                'Path does not exist: {}'.format(image_set_file)
        with open(image_set_file) as fp:
            image_index = [x.strip() for x in fp.readlines()]
        return image_index

    def gt_roidb(self):
        """
        Return the database of ground-truth regions of interest.

        This function loads/saves from/to a cache file to speed up future calls.
        """
        cache_file = os.path.join(self.cache_path, self.name + '_gt_roidb.pkl')
        if os.path.exists(cache_file):
            with open(cache_file, 'rb') as fid:
                roidb = pickle.load(fid)
            print ('{} gt roidb loaded from {}'.format(self.name, cache_file))
            return roidb

        gt_roidb = [self._load_my_dataset_annotation(index)
                    for index in self._image_index]
        with open(cache_file, 'wb') as fid:
            pickle.dump(gt_roidb, fid, pickle.HIGHEST_PROTOCOL)
        print ('wrote gt roidb to {}'.format(cache_file))

        return gt_roidb

    def _load_my_dataset_annotation(self, index):
        """
        Load image and bounding boxes info from txt file in YOLO
        format.
        """
        filename = index.strip()
        filename = filename.replace("images", "labels")
        filename = filename.replace(".jpg", ".txt")
        filename = filename.replace(".png", ".txt")
        print ("file: {}".format(filename))

        num_objs = 0
        objs = []
        with open(filename) as fp:
            for l in fp.readlines():
                obj = l.strip().split()
                num_objs += 1
                objs.append(obj)

        boxes = np.zeros((num_objs, 4), dtype=np.uint16)
        gt_classes = np.zeros((num_objs), dtype=np.int32)
        overlaps = np.zeros((num_objs, self.num_classes), dtype=np.float32)
        # "Seg" area for pascal is just the box area
        seg_areas = np.zeros((num_objs), dtype=np.float32)

        # Load object bounding boxes into a data frame.
        for ix, obj in enumerate(objs):
            #recupero i valori relativi alle mie annotazioni txt
            #dalla struttura riempita precedentemente           

            #QUI MI SERVONO ALTEZZA E LARGHEZZA DELLE IMMAGINI
            if filename.find("real") != -1:
                im_width = self.my_cfg["dati_reali"]["width"]
                im_heigth = self.my_cfg["dati_reali"]["height"]
            if filename.find("synth") != -1:
                im_width = self.my_cfg["dati_sintetici"]["width"]
                im_heigth = self.my_cfg["dati_sintetici"]["height"]
            
            #recupero i singoli valori da ogni riga delle annotazioni
            bb_list = obj;
            print (obj)

            name_index = bb_list[0]
            x_center = float(bb_list[1])
            y_center = float(bb_list[2])
            bb_width = float(bb_list[3])
            bb_heigth = float(bb_list[4])

            #ora devo calcolare i valori che servono a lui espressi in pixel
            #i valori delle cordinate trovate non possono uscire dall'immagine
            #e devono quindi essere compresi tra 0 e 1
            x1 = (x_center - (bb_width / 2)) * im_width
            y1 = (y_center - (bb_heigth / 2)) * im_heigth
            x2 = (x_center + (bb_width / 2)) * im_width
            y2 = (y_center + (bb_heigth / 2)) * im_heigth

            # bbox = obj.find('bndbox')
            # Make pixel indexes 0-based
            x1 = int(x1) - 1
            y1 = int(y1) - 1
            x2 = int(x2) - 1
            y2 = int(y2) - 1

            #controllo che i valori calcolati non siano
            #minori di 0 o maggiori di 1
            if x1 < 0: x1 = 0
            if x1 > im_width - 1: x1 = im_width - 1
            if y1 < 0: y1 = 0
            if y1 > im_heigth - 1: y1 = im_heigth - 1
            if x2 < 0: x2 = 0
            if x2 > im_width - 1: x2 = im_width - 1
            if y2 < 0: y2 = 0
            if y2 > im_heigth - 1: y2 = im_heigth - 1

            print("{} {} {} {}".format(x1, y1, x2, y2))

            #dobbiamo recuperare il nome della classe
            #ho aggiunto il +1 perchè le annotazioni in formato Yolo no tengono
            #in considerazione la presenza della classe background
            class_name = self._classes[int(name_index) + 1]
            cls = self._class_to_ind[class_name]

            boxes[ix, :] = [x1, y1, x2, y2]
            gt_classes[ix] = cls
            overlaps[ix, cls] = 1.0
            seg_areas[ix] = (x2 - x1 + 1) * (y2 - y1 + 1)

        overlaps = scipy.sparse.csr_matrix(overlaps)

        return {'boxes' : boxes,
                'gt_classes': gt_classes,
                'gt_overlaps' : overlaps,
                'flipped' : False,
                'seg_areas' : seg_areas}

    def _get_my_dataset_results_file_template(self):
        #andiamo a creare il path che conterra i risultati
        #un file txt per ogni classe
        filename = "./../../" + self._image_set + '_{:s}.txt'
        path = os.path.abspath(filename)
        return path

    def _write_my_dataset_results_file(self, all_boxes):
        for cls_ind, cls in enumerate(self.classes):
            if cls == '__background__':
                continue
            print ('Writing {} my_dataset results file'.format(cls))
            filename = self._get_my_dataset_results_file_template().format(cls)
            with open(filename, 'wt') as f:
                for im_ind, index in enumerate(self.image_index):
                    dets = all_boxes[cls_ind][im_ind]
                    if dets == []:
                        continue
                    # the VOCdevkit expects 1-based indices
                    for k in range(dets.shape[0]):
                        f.write('{:s} {:.3f} {:.1f} {:.1f} {:.1f} {:.1f}\n'.
                                format(index, dets[k, -1],
                                       dets[k, 0] + 1, dets[k, 1] + 1,
                                       dets[k, 2] + 1, dets[k, 3] + 1))

    def _do_python_eval(self, output_dir = './../../output'):

        imagesetfile = self._image_path_txt

        cachedir = os.path.join("./../../temp_file", 'annotations_cache')
        cachedir = os.path.abspath(cachedir)
        aps = []
        
        if not os.path.isdir(output_dir):
            os.mkdir(output_dir)
        for i, cls in enumerate(self._classes):
            if cls == '__background__':
                continue
            filename = self._get_my_dataset_results_file_template().format(cls)
            rec, prec, ap = my_dataset_eval(self.my_cfg,
                filename, imagesetfile, cls, cachedir, ovthresh=0.5)
            aps += [ap]
            print('AP for {} = {:.4f}'.format(cls, ap))
            with open(os.path.join(output_dir, cls + '_pr.pkl'), 'w') as f:
                pickle.dump({'rec': rec, 'prec': prec, 'ap': ap}, f)
        print('Mean AP = {:.4f}'.format(np.mean(aps)))
        print('~~~~~~~~')
        print('Results:')
        for ap in aps:
            print('{:.3f}'.format(ap))
        print('{:.3f}'.format(np.mean(aps)))
        print('~~~~~~~~')
        print('')
        print('--------------------------------------------------------------')
        print('Results computed with the **unofficial** Python eval code.')
        print('Results should be very close to the official MATLAB eval code.')
        print('Recompute with `./tools/reval.py --matlab ...` for your paper.')
        print('-- Thanks, The Management')
        print('--------------------------------------------------------------')

    def evaluate_detections(self, all_boxes, output_dir):
        self._write_my_dataset_results_file(all_boxes)
        self._do_python_eval(output_dir)
        if self.config['matlab_eval']:
            self._do_matlab_eval(output_dir)
        if self.config['cleanup']:
            for cls in self._classes:
                if cls == '__background__':
                    continue
                filename = self._get_my_dataset_results_file_template().format(cls)
                os.remove(filename)

    def competition_mode(self, on):
        if on:
            self.config['use_salt'] = False
            self.config['cleanup'] = False
        else:
            self.config['use_salt'] = True
            self.config['cleanup'] = True

    '''
    def image_path_from_index(self, index):
        """
        Construct an image path from the image's "index" identifier.
        """
        image_path = os.path.join(self._data_path, 'JPEGImages',
                                  index + self._image_ext)
        assert os.path.exists(image_path), \
                'Path does not exist: {}'.format(image_path)
        return image_path
    '''

    '''
    def _get_default_path(self):
            """
        Return the default path where PASCAL VOC is expected to be installed.
        """
        return os.path.join(cfg.DATA_DIR, 'VOCdevkit' + self._year)
    '''

    def _do_matlab_eval(self, output_dir='output'):
        '''
        print ('-----------------------------------------------------')
        print ('Computing results with the official MATLAB eval code.')
        print ('-----------------------------------------------------')
        path = os.path.join(cfg.ROOT_DIR, 'lib', 'datasets',
                            'VOCdevkit-matlab-wrapper')
        cmd = 'cd {} && '.format(path)
        cmd += '{:s} -nodisplay -nodesktop '.format(cfg.MATLAB)
        cmd += '-r "dbstop if error; '
        cmd += 'voc_eval(\'{:s}\',\'{:s}\',\'{:s}\',\'{:s}\'); quit;"' \
            .format(self._devkit_path, self._get_comp_id(),
                    self._image_set, output_dir)
        print('Running:\n{}'.format(cmd))
        status = subprocess.call(cmd, shell=True)
        '''
        raise ValueError("matlab_eval non implementata")

    def _get_comp_id(self):
        '''
        comp_id = (self._comp_id + '_' + self._salt if self.config['use_salt']
            else self._comp_id)
        return comp_id
        '''
        raise ValueError("matlab_eval non implementata")
    
    def rpn_roidb(self):
        '''
        if int(self._year) == 2007 or self._image_set != 'test':
            gt_roidb = self.gt_roidb()
            rpn_roidb = self._load_rpn_roidb(gt_roidb)
            roidb = imdb.merge_roidbs(gt_roidb, rpn_roidb)
        else:
            roidb = self._load_rpn_roidb(None)

        return roidb
        '''
        raise ValueError("NON DOVRESTI CHIAMARE rpn_roidb")
      
    def _load_rpn_roidb(self, gt_roidb):
        '''
        filename = self.config['rpn_file']
        print ('loading {}'.format(filename))
        assert os.path.exists(filename), \
            'rpn data not found at: {}'.format(filename)
        with open(filename, 'rb') as f:
            box_list = pickle.load(f)
        return self.create_roidb_from_box_list(box_list, gt_roidb)
        '''
        raise ValueError("NON DOVRESTI CHIAMARE _load_rpn_roidb")
    
    def selective_search_roidb(self):
        """
        Return the database of selective search regions of interest.
        Ground-truth ROIs are also included.

        This function loads/saves from/to a cache file to speed up future calls.
        """
        '''
        cache_file = os.path.join(self.cache_path,
                                  self.name + '_selective_search_roidb.pkl')

        if os.path.exists(cache_file):
            with open(cache_file, 'rb') as fid:
                roidb = pickle.load(fid, encoding='latin1')
            print ('{} ss roidb loaded from {}'.format(self.name, cache_file))
            return roidb

        if int(self._year) == 2007 or self._image_set != 'test':
            gt_roidb = self.gt_roidb()
            ss_roidb = self._load_selective_search_roidb(gt_roidb)
            roidb = imdb.merge_roidbs(gt_roidb, ss_roidb)
        else:
            roidb = self._load_selective_search_roidb(None)
        with open(cache_file, 'wb') as fid:
            pickle.dump(roidb, fid, pickle.HIGHEST_PROTOCOL)
        print ('wrote ss roidb to {}'.format(cache_file))

        return roidb
        '''
        raise ValueError("NON DOVRESTI CHIAMARE selective_search_roidb")

    def _load_selective_search_roidb(self, gt_roidb):
        '''
        filename = os.path.abspath(os.path.join(cfg.DATA_DIR,
                                                'selective_search_data',
                                                self.name + '.mat'))
        assert os.path.exists(filename), \
               'Selective search data not found at: {}'.format(filename)
        raw_data = sio.loadmat(filename)['boxes'].ravel()

        box_list = []
        for i in range(raw_data.shape[0]):
            boxes = raw_data[i][:, (1, 0, 3, 2)] - 1
            keep = ds_utils.unique_boxes(boxes)
            boxes = boxes[keep, :]
            keep = ds_utils.filter_small_boxes(boxes, self.config['min_size'])
            boxes = boxes[keep, :]
            box_list.append(boxes)

        return self.create_roidb_from_box_list(box_list, gt_roidb)
        '''
        raise ValueError("NON DOVRESTI CHIAMARE _load_selective_search_roidb")



if __name__ == '__main__':
    d = my_dataset('train')
