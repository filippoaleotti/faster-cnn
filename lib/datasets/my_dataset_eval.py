# --------------------------------------------------------
# Fast/er R-CNN
# Licensed under The MIT License [see LICENSE for details]
# Written by Bharath Hariharan
# --------------------------------------------------------

import xml.etree.ElementTree as ET
import os
import pickle
import numpy as np
import pdb

def parse_rec(filename, configuration):
    """ Eseguo il parse del file txt
        in formato Yolo """
    objects = []

    if (filename.find("real") != -1):
        im_width = configuration["dati_reali"]["width"]
        im_heigth = configuration["dati_reali"]["height"]
    if (filename.find("synth") != -1):
        im_width = configuration["dati_sintetici"]["width"]
        im_heigth = configuration["dati_sintetici"]["height"]

    with os.open(filename) as f:
        lines = f.readlines
        for line in lines:
            obj_struct = {}
            splitline = line.strip().split(' ')
            name = configuration["classes"][splitline[0]]
            obj_struct['name'] = name

            x_center = float(splitline[1])
            y_center = float(splitline[2])
            bb_width = float(splitline[3])
            bb_heigth = float(splitline[4])

            #ora devo calcolare i valori che servono a lui espressi in pixel
            x1 = (x_center - (bb_width / 2)) * im_width
            y1 = (y_center - (bb_heigth / 2)) * im_heigth
            x2 = (x_center + (bb_width / 2)) * im_width
            y2 = (y_center + (bb_heigth / 2)) * im_heigth

            obj_struct['bbox'] = [int(x1),
                                  int(y1),
                                  int(x2),
                                  int(y2)]

            objects.append(obj_struct)

    return objects

def my_dataset_ap(rec, prec):
    # correct AP calculation
    # first append sentinel values at the end
    mrec = np.concatenate(([0.], rec, [1.]))
    mpre = np.concatenate(([0.], prec, [0.]))

    # compute the precision envelope
    for i in range(mpre.size - 1, 0, -1):
        mpre[i - 1] = np.maximum(mpre[i - 1], mpre[i])

    # to calculate area under PR curve, look for points
    # where X axis (recall) changes value
    i = np.where(mrec[1:] != mrec[:-1])[0]

    # and sum (\Delta recall) * prec
    ap = np.sum((mrec[i + 1] - mrec[i]) * mpre[i + 1])
    return ap

def my_dataset_eval(configuration,
                    detpath,
                    imagesetfile,
                    classname,
                    cachedir,
                    ovthresh=0.5):

    """rec, prec, ap = voc_eval(detpath,
                                annopath,
                                imagesetfile,
                                classname,
                                [ovthresh],
                                [use_07_metric])

    Top level function that does the PASCAL VOC evaluation.

    detpath: Path to detections
    detpath.format(classname) should produce the detection results file.
    imagesetfile: Text file containing the list of images, one image per line.

    classname: Category name (duh)
    cachedir: Directory for caching the annotations
    [ovthresh]: Overlap threshold (default = 0.5)
    [use_07_metric]: Whether to use VOC07's 11 point AP computation
        (default False)
    """
    # assumes detections are in detpath.format(classname)
    # assumes annotations are in annopath.format(imagename)
    # assumes imagesetfile is a text file with each line an image name
    # cachedir caches the annotations in a pickle file

    # first load gt
    if not os.path.isdir(cachedir):
        os.mkdir(cachedir)
    cachefile = os.path.join(cachedir, 'annots.pkl')

    # read list of images
    with open(imagesetfile, 'r') as f:
        lines = f.readlines()
    imagenames = [x.strip() for x in lines]


    if not os.path.isfile(cachefile):
        # load annots
        recs = {}
        for i, imagename in enumerate(imagenames):
            if (imagename.find("real") != -1):
                image_ext = configuration["dati_reali"]["extension"]
            else:
                image_ext = configuration["dati_reali"]["extension"]

            annopath = imagename.replace("images", "labels").replace(image_ext, ".txt")
            recs[imagename] = parse_rec(annopath, configuration)
            
        # save
        print ('Saving cached annotations to {:s}'.format(cachefile))
        with open(cachefile, 'w') as f:
            pickle.dump(recs, f)
    else:
        # load
        with open(cachefile, 'r') as f:
            recs = pickle.load(f)

    # extract gt objects for this class
    class_recs = {}
    npos = 0
    for imagename in imagenames:
        R = [obj for obj in recs[imagename] if obj['name'] == classname]
        bbox = np.array([x['bbox'] for x in R])
        det = [False] * len(R)
        class_recs[imagename] = {'bbox': bbox,
                                 'det': det}

    # read dets
    detfile = detpath.format(classname)
    with open(detfile, 'r') as f:
        lines = f.readlines()
    if any(lines) == 1:

        splitlines = [x.strip().split(' ') for x in lines]
        image_ids = [x[0] for x in splitlines]
        confidence = np.array([float(x[1]) for x in splitlines])
        BB = np.array([[float(z) for z in x[2:]] for x in splitlines])

        # sort by confidence
        sorted_ind = np.argsort(-confidence)
        sorted_scores = np.sort(-confidence)
        BB = BB[sorted_ind, :]
        image_ids = [image_ids[x] for x in sorted_ind]

        # go down dets and mark TPs and FPs
        nd = len(image_ids)
        tp = np.zeros(nd)
        fp = np.zeros(nd)
        for d in range(nd):
            R = class_recs[image_ids[d]]
            bb = BB[d, :].astype(float)
            ovmax = -np.inf
            BBGT = R['bbox'].astype(float)

            if BBGT.size > 0:
                # compute overlaps
                # intersection
                ixmin = np.maximum(BBGT[:, 0], bb[0])
                iymin = np.maximum(BBGT[:, 1], bb[1])
                ixmax = np.minimum(BBGT[:, 2], bb[2])
                iymax = np.minimum(BBGT[:, 3], bb[3])
                iw = np.maximum(ixmax - ixmin + 1., 0.)
                ih = np.maximum(iymax - iymin + 1., 0.)
                inters = iw * ih

                # union
                uni = ((bb[2] - bb[0] + 1.) * (bb[3] - bb[1] + 1.) +
                       (BBGT[:, 2] - BBGT[:, 0] + 1.) *
                       (BBGT[:, 3] - BBGT[:, 1] + 1.) - inters)

                overlaps = inters / uni
                ovmax = np.max(overlaps)
                jmax = np.argmax(overlaps)

            if ovmax > ovthresh:
                #if not R['difficult'][jmax]:
                if not R['det'][jmax]:
                    tp[d] = 1.
                    R['det'][jmax] = 1
                else:
                    fp[d] = 1.
            else:
                fp[d] = 1.

        # compute precision recall
        fp = np.cumsum(fp)
        tp = np.cumsum(tp)
        rec = tp / float(npos)
        # avoid divide by zero in case the first detection matches a difficult
        # ground truth
        prec = tp / np.maximum(tp + fp, np.finfo(np.float64).eps)
        ap = my_dataset_ap(rec, prec)
    else:
         rec = -1
         prec = -1
         ap = -1

    return rec, prec, ap
