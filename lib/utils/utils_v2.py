from xml.dom import minidom

import sys
import os
from contextlib import contextmanager
import subprocess
import json 

@contextmanager
def cd(newdir):
    prevdir = os.getcwd()
    os.chdir(os.path.expanduser(newdir))
    try:
        yield
    finally:
        os.chdir(prevdir)

class DatiClass:
    def __init__ (self, width, height, extension):
        self.width = width
        self.height = height
        self.extension = extension

def get_configuration_parameters():
    cfg = dict()
    configuration_file_path = os.path.abspath("./configuration/configuration.json")
    with open(configuration_file_path, "r") as f:
        cfg = json.load(f)
    classes = len(cfg["classes"])
    filters = (5 + classes) * 5
    cfg["filters"] = filters
    cfg["classnum"] = classes
    return cfg

def write(cfg):
    json_string = json.dumps(cfg, sort_keys=False, indent=4)
    with open("./../../configuration/configuration.json", "w") as f:
        f.write(json_string)


def change_file(dest_path,input_path,patterns,configuration):
    """
        La funzione prende in ingresso dei pattern e controlla se il file li contiene
        Se una riga contiene il patter allora lo sostituisce con il valore corrispondente
        recuperandolo dal dizionario configuration.
        Ad esempio, definendo **BATCH** come pattern, lo script cambiera' la riga
        batch =**BATCH** con batch = 64

    """
    if os.path.isfile(dest_path):
        os.remove(dest_path)
    with open(dest_path,'w+') as temp:
        with open(input_path,"r") as f:
            for line in f:
                for chiave in patterns.keys():
                    if line.find(chiave)!= -1:
                        line = line.replace(chiave, str(patterns[chiave]))    
                temp.write(line)   

def run(path,cmd):                
    with cd(path):
        subprocess.call(cmd)

if __name__ == "__main__":
    d = dict()
    #d["cnn"] = "yolo"
    #d["yolo"] = "/home/lorenzo/darknet"
    d["batch"] = 32
    d["subdivisions"] = 8
    d["classes"] = ["emptySpace", "runningLow"]
    #d["training_perc"] = 0.1
    #d["config_dir"] = "/home/lorenzo/Scrivania/prova_script_V2/script_config/config"
    #d["test_dir"] = "/home/lorenzo/Scrivania/prova_script_V2/test"
    dati_sintetici = DatiClass(640.0, 480.0, ".png")
    d["dati_sintetici"] = dati_sintetici.__dict__
    dati_reali = DatiClass(640.0, 480.0, ".jpg")
    d["dati_reali"] = dati_reali.__dict__
    #d["new_dir_path"] = "/home/lorenzo/Scrivania/prova_script_V2/myDir"
    d["epoch"] = 30
    #d["synt_labels_file_prefix"] = "bb_frame"

    d["test_image_txt"] = "test.txt"
    d["train_image_txt"] = "train.txt"
    d["valid_image_txt"] = "valid.txt"

    write(d)