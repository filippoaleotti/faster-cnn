# Faster R-CNN Tensorflow
Versione sperimentale di Faster R-CNN implementata in Tensorflow.
Per dettagli riguardo R-CNN controllare il paper  [Faster R-CNN: Towards Real-Time Object Detection with Region Proposal Networks](http://arxiv.org/pdf/1506.01497v3.pdf) di Shaoqing Ren, Kaiming He, Ross Girshick, Jian Sun.
Questa versione, rispetto all'[originale] (https://github.com/smallcorgi/Faster-RCNN_TF) è stata modificata per riuscire ad effettuare il training e il test usando un proprio dataset
e annotazioni salvate con lo stesso formato Yolo (class x_center y_center width height) invece che annotazioni XML.

### Requisiti: software

1. [Tensorflow](https://www.tensorflow.org/))

2. Python packages: `cython`, `python-opencv`, `easydict`

### Requisiti: hardware

1. 3G di memoria GPU è sufficiente (con CUDNN)

### Installazione

1. Clone del repository
  
```Shell
  git clone https://gitlab.com/filippoaleotti/faster-cnn
```

2. Build dei moduli Cython
    
```Shell
    cd $FRCN_ROOT/lib
    make
```

### Training

1. Configurare opportunamente la rete cambiando il file di configurazione *configuration.json*
Questo file contiene varie voci:  
    - **batch:** numero di immagini che vengono analizzate prima di aggiornare i pesi.
    - **new_dir_path:** percorso alla cartella che conterrà i link simbolici alle immagini e i nuovi file di labels (per motivi di gestione dello spazio nella nuova cartella non verranno copiate le immagini del dataset ma bensì verranno creati dei link simbolici).
    - **test_dir:** percorso alla cartella che contiene le immagini ed i relativi dati che verranno utilizzate per la fase di testing finale. (Questa cartella dovrà avere una struttura interna analoga a quella delle immagini reali che verrà descritta successivamente).
    - **config_dir:** percorso alla cartella di configurazione (che contiene i file template *obj.data* e *yolo-obj.cfg*).
    - **classes:** nome delle classi da utilizzare durante la fase di training, validazione e test della rete.
    - **cnn:** tipo di cnn. Impostare "faster_rcnn"
    - **training_perc:** percentuale del dataset che verrà destinata alla fase di training (la restante parte verrà utilizzata per la fase di validazione).  
    Nel caso di 0.9, ad esempio, di tutto il dataset il 90% delle immagini sarà usato per il training e solo il 10% per la validazione.
    - **epoch:** numero di epoche che dovrà essere utilizzato per il train della rete.
    -**synt_labels_file_prefix:** prefisso del nome del file di annotazione da considerare per le immagini sintetiche (questo nome verrà poi sostituito dalla parola 'frame' nel nome del file di label di destinazione, è presente un esempio nelle sezioni successive del documento).
    - **dati reali** e **dati sintetici** permettono invece di configurare i parametri di gestione delle immagini reali e sinteticche che costituiscono il dataset.  
    Per entrambi bisogna fornire:  
        - **right:** indice se nel dataset sono presenti anche le immagini di destra (camera stereo), oppure solo quelle di sinistra/camera mono.
        - **extension:** cioè l'estensione dei file immagine.
        - **percent:** percentuale dei dati da considerare. Se percent vale 0.5 ad esempio solo metà del dataset verrà considerato per il train e la validazione della rete. 
        - **height:** altezza espressa in pixel delle immagini del dataset (ad esempio 480).
        - **width:**larghezza espressa in pixel delle immagini del dataset (ad esempio 640).
        - **disparity:** nel caso in cui il flag di *right* sia a true, questo parametro indica la disparità della camera utilizzata.
        - **path:** percorso al dataset.
    - **faster_rcnn**: permette di configurare Faster R-CNN. In particolare modificare opportunamente
        - **directory** percorso alla cartella di faster nel proprio file system
        - **iou_th** soglia utilizzata durante i test

2. Scaricare i pesi per il training da [Drive](https://drive.google.com/file/d/0ByuDEGFYmWsbNVF5eExySUtMZmM/view) o [Dropbox](https://www.dropbox.com/s/po2kzdhdgl4ix55/VGG_imagenet.npy?dl=0)

3. Aprire un terminale e lanciare lo script
    
```sh
    python3 cfg_yolo_v5.py 
```
    
Lo script realizzato andrà a popolare la directory specificata nel file di configurazione a partire dalle cartelle che costituiscono il dataset.
La directory di output conterrà tre cartelle separate per i dati di training, validazione e test. 
Lo script si occuperà di generare i file txt necessari per le tre fasi citate precedentemente ed avvierà il training della rete.



### Modifiche rispetto alla versione originale

Rispetto alla versione originale, è stata implementata una classe python chiamata my\_dataset nel file */lib/dataset/my_dataset* per modellare il nuovo dataset
e una classe chiamata my\_dataset_eval nel file */lib/dataset/my_dataset* in grado di leggere i file txt contenenti le annotazioni e convertirli nel formato utilizzato internamente da Faster R-CNN.
Il file *factory.py* è stato modificato per poter istanziare anche il nuovo dataset.

